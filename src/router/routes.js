const routes = [
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
  {
    name: 'Login',
    path: "/login", component: () => import("pages/Login.vue"),
  },
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/", component: () => import("pages/Index.vue"),
        meta: {requiredLogin: true},
      },
      {
        path: "/about", component: () => import("pages/About.vue"),
        meta: {requiredLogin: true},
      },
      {
        path: "/help", component: () => import("pages/Help.vue"),
        meta: {requiredLogin: true},
      },
    ],
  },
];

export default routes;
