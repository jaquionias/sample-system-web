import { api } from "boot/axios";

export const doLogin = async ({commit}, payload) => {
  await api.post('/auth/login', payload).then(respose => {
    const token = respose.data;
    commit('setToken', token);
    api.defaults.headers.common.Authorization = 'Bearer ' + token.access_token;
  });
};

export const signOut = ({commit}) => {
  api.defaults.headers.common.Authorization = '';
  commit('removeToken');
};

export const getMe = async ({commit, dispatch}, token) => {

};

export const init = async ({commit}) => {
  const token = localStorage.getItem('token');
  if (token) {
    await commit('setToken', JSON.parse(token));
  } else {
    commit('removeToken');
  }
};

